export class CreateDeliveryDto {
  name: string;
  phone: string;
  cedula: string;
  place: string;
  state: boolean
}

export class CreateAgentDto {
  name: string
  price: number
}
