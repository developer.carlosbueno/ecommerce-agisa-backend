export class Delivery {
  _id: string
  name: string;
  phone: string;
  cedula: string;
  place: string;
  state: boolean
}
